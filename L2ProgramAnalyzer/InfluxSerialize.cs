﻿using System;
using System.Collections.Generic;

namespace L2ProgramAnalyzer
{


    public class InfluxSeries
    {
        public List<List<double>> Values { get; set; }
        public string Name { get; set; }
        public List<string> Columns { get; set; }

        public List<InfluxValues> ListValues {
            get {
                List<InfluxValues> list = new List<InfluxValues>();
                foreach (var x in Values)
                {
                    list.Add(new InfluxValues
                    {
                        _time = x[0],
                        Value = x[1]
                    });
                }
                return list;
            }
        }



    }


    public class InfluxValues
    {
        public double _time { get; set; }

        public DateTime Time
        {
            get
            {
                DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
                dtDateTime = dtDateTime.AddSeconds((double)_time).ToLocalTime();
                return dtDateTime;
            }
        }

        public double Value { get; set; }
    }
}
