﻿namespace L2ProgramAnalyzer
{
    partial class MainForm
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.chartGraph = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.nameFile = new System.Windows.Forms.TextBox();
            this.summerTime = new System.Windows.Forms.CheckBox();
            this.downloadFile = new System.Windows.Forms.Button();
            this.toDatePicker = new System.Windows.Forms.DateTimePicker();
            this.fromDatePicker = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.refreshMeters = new System.Windows.Forms.Button();
            this.listMeters = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.refreshID = new System.Windows.Forms.Button();
            this.comboID = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.cartesianChart1 = new LiveCharts.WinForms.CartesianChart();
            this.label6 = new System.Windows.Forms.Label();
            this.meanChecked = new System.Windows.Forms.CheckBox();
            this.epochChecked = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.epochChecked);
            this.groupBox1.Controls.Add(this.meanChecked);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.comboBox1);
            this.groupBox1.Controls.Add(this.chartGraph);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.nameFile);
            this.groupBox1.Controls.Add(this.summerTime);
            this.groupBox1.Controls.Add(this.downloadFile);
            this.groupBox1.Controls.Add(this.toDatePicker);
            this.groupBox1.Controls.Add(this.fromDatePicker);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.refreshMeters);
            this.groupBox1.Controls.Add(this.listMeters);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.refreshID);
            this.groupBox1.Controls.Add(this.comboID);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(302, 402);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Opciones";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "JSON",
            "CSV"});
            this.comboBox1.Location = new System.Drawing.Point(38, 323);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(68, 21);
            this.comboBox1.TabIndex = 14;
            // 
            // chartGraph
            // 
            this.chartGraph.Location = new System.Drawing.Point(20, 357);
            this.chartGraph.Name = "chartGraph";
            this.chartGraph.Size = new System.Drawing.Size(75, 23);
            this.chartGraph.TabIndex = 13;
            this.chartGraph.Text = "Graficar";
            this.chartGraph.UseVisualStyleBackColor = true;
            this.chartGraph.Click += new System.EventHandler(this.chartGraph_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 297);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(86, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Nombre Archivo:";
            // 
            // nameFile
            // 
            this.nameFile.Location = new System.Drawing.Point(101, 297);
            this.nameFile.Name = "nameFile";
            this.nameFile.Size = new System.Drawing.Size(162, 20);
            this.nameFile.TabIndex = 11;
            // 
            // summerTime
            // 
            this.summerTime.AutoSize = true;
            this.summerTime.Location = new System.Drawing.Point(9, 267);
            this.summerTime.Name = "summerTime";
            this.summerTime.Size = new System.Drawing.Size(97, 17);
            this.summerTime.TabIndex = 10;
            this.summerTime.Text = "Horario Verano";
            this.summerTime.UseVisualStyleBackColor = true;
            // 
            // downloadFile
            // 
            this.downloadFile.Location = new System.Drawing.Point(101, 357);
            this.downloadFile.Name = "downloadFile";
            this.downloadFile.Size = new System.Drawing.Size(75, 23);
            this.downloadFile.TabIndex = 6;
            this.downloadFile.Text = "Descargar";
            this.downloadFile.UseVisualStyleBackColor = true;
            this.downloadFile.Click += new System.EventHandler(this.button1_Click);
            // 
            // toDatePicker
            // 
            this.toDatePicker.CustomFormat = "dd-MM-yyyy HH:mm:ss";
            this.toDatePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.toDatePicker.Location = new System.Drawing.Point(53, 241);
            this.toDatePicker.Name = "toDatePicker";
            this.toDatePicker.Size = new System.Drawing.Size(210, 20);
            this.toDatePicker.TabIndex = 9;
            // 
            // fromDatePicker
            // 
            this.fromDatePicker.CustomFormat = "dd-MM-yyyy HH:mm:ss";
            this.fromDatePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.fromDatePicker.Location = new System.Drawing.Point(53, 204);
            this.fromDatePicker.Name = "fromDatePicker";
            this.fromDatePicker.Size = new System.Drawing.Size(210, 20);
            this.fromDatePicker.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 241);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Hasta:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 204);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Desde:";
            // 
            // refreshMeters
            // 
            this.refreshMeters.Location = new System.Drawing.Point(269, 63);
            this.refreshMeters.Name = "refreshMeters";
            this.refreshMeters.Size = new System.Drawing.Size(27, 21);
            this.refreshMeters.TabIndex = 5;
            this.refreshMeters.Text = "🔄";
            this.refreshMeters.UseVisualStyleBackColor = true;
            this.refreshMeters.Visible = false;
            this.refreshMeters.Click += new System.EventHandler(this.refreshMeters_Click);
            // 
            // listMeters
            // 
            this.listMeters.FormattingEnabled = true;
            this.listMeters.Location = new System.Drawing.Point(76, 63);
            this.listMeters.Name = "listMeters";
            this.listMeters.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.listMeters.Size = new System.Drawing.Size(187, 134);
            this.listMeters.Sorted = true;
            this.listMeters.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Mediciones:";
            // 
            // refreshID
            // 
            this.refreshID.Location = new System.Drawing.Point(269, 26);
            this.refreshID.Name = "refreshID";
            this.refreshID.Size = new System.Drawing.Size(27, 21);
            this.refreshID.TabIndex = 2;
            this.refreshID.Text = "🔄";
            this.refreshID.UseVisualStyleBackColor = true;
            this.refreshID.Click += new System.EventHandler(this.refreshID_Click);
            // 
            // comboID
            // 
            this.comboID.FormattingEnabled = true;
            this.comboID.Location = new System.Drawing.Point(33, 26);
            this.comboID.Name = "comboID";
            this.comboID.Size = new System.Drawing.Size(230, 21);
            this.comboID.Sorted = true;
            this.comboID.TabIndex = 1;
            this.comboID.SelectedIndexChanged += new System.EventHandler(this.refreshMeters_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "ID:";
            // 
            // chart1
            // 
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chart1.Legends.Add(legend1);
            this.chart1.Location = new System.Drawing.Point(334, 12);
            this.chart1.Name = "chart1";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.chart1.Series.Add(series1);
            this.chart1.Size = new System.Drawing.Size(170, 114);
            this.chart1.TabIndex = 1;
            this.chart1.Text = "chart1";
            // 
            // cartesianChart1
            // 
            this.cartesianChart1.Location = new System.Drawing.Point(334, 12);
            this.cartesianChart1.Name = "cartesianChart1";
            this.cartesianChart1.Size = new System.Drawing.Size(641, 402);
            this.cartesianChart1.TabIndex = 2;
            this.cartesianChart1.Text = "cartesianChart1";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 326);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(31, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "Tipo:";
            // 
            // meanChecked
            // 
            this.meanChecked.AutoSize = true;
            this.meanChecked.Location = new System.Drawing.Point(112, 267);
            this.meanChecked.Name = "meanChecked";
            this.meanChecked.Size = new System.Drawing.Size(70, 17);
            this.meanChecked.TabIndex = 16;
            this.meanChecked.Text = "Promedio";
            this.meanChecked.UseVisualStyleBackColor = true;
            // 
            // epochChecked
            // 
            this.epochChecked.AutoSize = true;
            this.epochChecked.Location = new System.Drawing.Point(183, 267);
            this.epochChecked.Name = "epochChecked";
            this.epochChecked.Size = new System.Drawing.Size(57, 17);
            this.epochChecked.TabIndex = 17;
            this.epochChecked.Text = "Epoch";
            this.epochChecked.UseVisualStyleBackColor = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(987, 426);
            this.Controls.Add(this.cartesianChart1);
            this.Controls.Add(this.chart1);
            this.Controls.Add(this.groupBox1);
            this.Name = "MainForm";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button refreshID;
        private System.Windows.Forms.ComboBox comboID;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button refreshMeters;
        private System.Windows.Forms.ListBox listMeters;
        private System.Windows.Forms.Button downloadFile;
        private System.Windows.Forms.DateTimePicker toDatePicker;
        private System.Windows.Forms.DateTimePicker fromDatePicker;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox summerTime;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox nameFile;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.Button chartGraph;
        private LiveCharts.WinForms.CartesianChart cartesianChart1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.CheckBox meanChecked;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox epochChecked;
    }
}

