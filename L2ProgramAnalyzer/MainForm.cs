﻿using LiveCharts;
using LiveCharts.Configurations;
using LiveCharts.Wpf;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;
using System.Windows.Forms;
using System.Windows.Media;
using Color = System.Drawing.Color;
using Point = System.Windows.Point;

namespace L2ProgramAnalyzer
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        public WebClient wClient = new WebClient();
        public NameValueCollection Querys = HttpUtility.ParseQueryString(string.Empty);
        public UriBuilder Url = new UriBuilder("http", "domergy.cl", 8086, "query", "?");
        public long Interval = 0; 

        private void refreshID_Click(object sender, EventArgs e)
        {
            comboID.Text = "";
            comboID.Items.Clear();
            listMeters.Items.Clear();

            Querys["db"] = "pird";
            Querys["q"] = "SHOW TAG VALUES WITH KEY = device";
            Url.Query = Querys.ToString();
            Querys.Clear();

            String jsonIDs = new StreamReader(wClient.OpenRead(Url.Uri)).ReadToEnd();
            var jParse = JObject.Parse(jsonIDs);
            foreach (var x in jParse["results"].First["series"])
            {
                foreach (var j in x["values"])
                {
                    if (!comboID.Items.Contains(j[1].ToString()))
                    {
                        comboID.Items.Add(j[1].ToString());
                    }
                }
            }

        }

        private void refreshMeters_Click(object sender, EventArgs e)
        {
            try
            {
                listMeters.Items.Clear();

                Querys["db"] = "pird";
                Querys["q"] = $"SHOW MEASUREMENTS WHERE device = '{comboID.SelectedItem}'";
                Url.Query = Querys.ToString();
                Querys.Clear();


                String jsonIDs = new StreamReader(wClient.OpenRead(Url.Uri)).ReadToEnd();
                var jParse = JObject.Parse(jsonIDs);
                foreach (var x in jParse["results"].First["series"])
                {
                    foreach (var j in x["values"])
                    {
                        if (!listMeters.Items.Contains(j.First.ToString()))
                        {
                            listMeters.Items.Add(j.First.ToString());
                        }
                    }
                }
            } catch (NullReferenceException)
            {
                comboID.Text = "";
                MessageBox.Show("Debe elegir algun dispositivo...");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (comboID.Items.Count < 1) return;
            if (listMeters.SelectedItems.Count < 1) return;

            string _url = urlQuery();

            if (!epochChecked.Checked)
            {
                _url = _url.Replace("&epoch=s", "");
            }

            if (comboBox1.SelectedItem.ToString().Equals("CSV"))
            {
                wClient.Headers.Set("Accept", "application/csv");
                wClient.DownloadFile(_url, "./" + nameFile.Text + ".csv");
            }
            else if (comboBox1.SelectedItem.ToString().Equals("JSON"))
            {
                wClient.DownloadFile(_url, "./" + nameFile.Text + ".json");
            }
        }

        private void chartGraph_Click(object sender, EventArgs e)
        {
            if (comboID.Items.Count < 1) return;
            if (listMeters.SelectedItems.Count < 1) return;
            if (listMeters.SelectedItems.Count >= 2) return;

            try
            {
                var data = JObject.Parse(wClient.DownloadString(urlQuery()));
                var jsonSeries = data["results"].First["series"].ToString();
                InfluxSeries[] series = new JavaScriptSerializer().Deserialize<InfluxSeries[]>(jsonSeries);

                var _Values = new ChartValues<double>();
                var _Times = new ChartValues<DateTime>();
                var _Test = new ChartValues<InfluxValues>();


                cartesianChart1.AxisX.Clear();
                var dayConfig = Mappers.Xy<InfluxValues>()
                .X(dayModel => (double)dayModel.Time.Ticks / TimeSpan.FromSeconds(1).Ticks)
                .Y(dayModel => dayModel.Value);


                var gradientBrush = new System.Windows.Media.LinearGradientBrush
                {
                    StartPoint = new System.Windows.Point(0, 0),
                    EndPoint = new Point(0, 1)
                };
                gradientBrush.GradientStops.Add(new GradientStop(System.Windows.Media.Color.FromRgb(33, 148, 241), 0));
                gradientBrush.GradientStops.Add(new GradientStop(Colors.Transparent, 1));


                var _Name = "";
                var _max = new InfluxValues();

                foreach (var item in series)
                {
                    _Name = item.Name;
                    foreach (var i in item.ListValues)
                    {
                        _Test.Add(i);
                    }

                }

                cartesianChart1.Series = new SeriesCollection(dayConfig) {
                        new LineSeries
                        {
                            Values = _Test,
                            Fill = gradientBrush,
                            StrokeThickness = 1,
                            PointGeometry = null,
                            Title = _Name
                        }
                };


                cartesianChart1.AxisX.Add(new Axis
                {
                    LabelFormatter = value => Functions.DateToString(value, "HH:mm:ss", summerTime.Checked)
                });


                /*chart1.Series.Clear();

                foreach (var item in series)
                {
                    chart1.Series.Add(item.Name);
                    chart1.Series[item.Name].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
                    if (Interval <= 64800)
                    {
                        chart1.ChartAreas[0].AxisX.LabelStyle.Format = "HH:mm:ss";
                    } else
                    {
                        chart1.ChartAreas[0].AxisX.LabelStyle.Format = "dd/mm HH:mm:ss";
                    }
                    chart1.ChartAreas[0].AxisX.LabelStyle.Angle = 90;

                    foreach (var i in item.ListValues)
                    {
                        chart1.Series[item.Name].Points.AddXY(i.Time, i.Value);
                    }
                }*/

                //}
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("El periodo desingado no posee mediciones.");
            }
            catch (ArgumentException)
            {
                MessageBox.Show("Imposible generar una vista, descargue los datos para trabajarlos de mejor manera.");
            }

        }


        private string urlQuery()
        {

            List<string> list_meter = new List<string>();
            foreach (var i in listMeters.SelectedItems)
            {
                list_meter.Add('"' + i.ToString() + '"');
            }

            string _meters = string.Join(", ", list_meter);
            string _device = comboID.SelectedItem.ToString();
            long _from = Functions.ConvertToUnixTime(DateTime.Parse(fromDatePicker.Text), summerTime.Checked);
            long _to = Functions.ConvertToUnixTime(DateTime.Parse(toDatePicker.Text), summerTime.Checked);
            Interval = _to - _from;

            double _Interval;

            if (Interval <= 3600)
            {
                _Interval = 60;
            }
            else if (Interval <= 6*3600)
            {
                _Interval = 200;
            }
            else if (Interval <= 12 * 3600)
            {
                _Interval = 800;
            }
            else
            {
                _Interval = 12000;
            }

            

            Querys["db"] = "pird";
            Querys["epoch"] = "s";

            if (meanChecked.Checked)
            {
                Querys["q"] = $"SELECT mean(value) FROM _meters WHERE (device = '{_device}') AND time >= {_from}s and time <= {_to}s GROUP BY time({_Interval}s)";
            }
            else
            {
                Querys["q"] = $"SELECT value FROM _meters WHERE (device = '{_device}') AND time >= {_from}s and time <= {_to}s";
            }
            

            Url.Query = Querys.ToString();
            Console.WriteLine(Url.Query);
            string URLQuery = Url.ToString().Replace("_meters", _meters);
            return URLQuery;



        }
    }
}
