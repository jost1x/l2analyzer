﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace L2ProgramAnalyzer
{
    class Functions
    {
        public static long ConvertToUnixTime(DateTime datetime, bool time)
        {
            var sTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Local);
            return !time ? (long)((datetime.AddSeconds(4 * 3600) - sTime)).TotalSeconds : (long)((datetime.AddSeconds(3 * 3600) - sTime)).TotalSeconds;
        }

        public static DateTime UnixToDate(long unix, bool time)
        {
            var dt = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Local).AddSeconds(unix);
            return !time ? dt.AddSeconds(-4 * 3600) : dt.AddSeconds(-3 * 3600);
        }

        internal static string DateToString(double value, string v, bool time)
        {
            var dt = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Local).AddSeconds((long) value);
            return dt.ToString(v);
        }
    }
}
